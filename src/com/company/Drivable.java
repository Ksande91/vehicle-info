package com.company;

public interface Drivable {
    double MAX_SPEED_CAR = 250.00;
    double MAX_SPEED_BIKE = 100.00;

    void accelerate(double speedFactor);

    void breaks(double speedFactor);

    void stop();
}
