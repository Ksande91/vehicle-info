package com.company;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Car extends Vehicle {

    private int power;
    private Calendar productionDate;

    public Car(){

    }

    public Car(int power, String color, String name, String serialNumber, int model, int price, int direction, int prodYear, int prodMonth, int prodDay){
        super(color, name, serialNumber, model, price, direction);
        this.power = power;
        productionDate = new GregorianCalendar(prodYear,prodMonth,prodDay);
        setBuyingDate(Calendar.getInstance());
    }


    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public void setAllFields(){
        super.setAllFields();
        System.out.println("Power: ");
        setPower(input.nextInt());
        System.out.println("Production date:");
        System.out.println("Year: ");
        int year = input.nextInt();
        System.out.println("Month: ");
        int month = input.nextInt()-1;
        System.out.println("Day of month: ");
        int dayOfMonth = input.nextInt();

        GregorianCalendar date = new GregorianCalendar(year, month, dayOfMonth);
        productionDate = date;
        setBuyingDate(Calendar.getInstance());
    }

    public Calendar getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(Calendar productionDate) {
        this.productionDate = productionDate;
    }

    public void turnRight(int degrees){
        if(degrees > 0 && degrees < 360){
            if(degrees + getDirection() > 360){
                setDirection(degrees+getDirection() - 360);
            }
            else{
                setDirection(degrees + getDirection());
            }
        }
    }

    public void turnLeft(int degrees){
        if(degrees > 0 && degrees < 360){
            if(getDirection() - degrees < 0){
                setDirection(getDirection()-degrees);
            }
            else{
                setDirection(getDirection()-degrees);
            }
        }
    }

    @Override
    public String toString(){
        return super.toString() + "Power: " + power;
    }

    public void accelerate(double factor){

        if(getSpeed() == 0){
            setSpeed(0.5*factor);

            if(getSpeed()>MAX_SPEED_CAR){
                setSpeed(MAX_SPEED_CAR);
            }

            System.out.printf("Vehicle accelerated to: %.2f km/h \n", getSpeed());

        }else{
            setSpeed(getSpeed()*factor);

            if(getSpeed()>MAX_SPEED_CAR){
                setSpeed(MAX_SPEED_CAR);
            }

            System.out.printf("Vehicle accelerated to: %.2f km/h \n", getSpeed());
        }
    }

    public void breaks(double factor){
        setSpeed(getSpeed()/factor);
        System.out.printf("Vehicle slowed down to: %.2f \n", getSpeed());
    }

    @Override
    public void readData(String[] parts){
        this.setName(parts[1]);
        this.setColor(parts[2]);
        this.setPrice(Integer.parseInt(parts[3]));
        this.setModel(Integer.parseInt(parts[4]));
        this.setSerialNumber(parts[5]);
        this.setDirection(Integer.parseInt(parts[6]));
        this.setPower(Integer.parseInt(parts[7]));

        GregorianCalendar date = new GregorianCalendar(Integer.parseInt(parts[8]), Integer.parseInt(parts[9]), Integer.parseInt(parts[10]));
        this.setProductionDate(date);

        GregorianCalendar buydate = new GregorianCalendar(Integer.parseInt(parts[11]), Integer.parseInt(parts[12]), Integer.parseInt(parts[13]));
        this.setBuyingDate(buydate);

        int prodYear = getProductionDate().get(Calendar.YEAR);
        int prodMonth = getProductionDate().get(Calendar.MONTH);
        int prodDay = getProductionDate().get(Calendar.DAY_OF_MONTH);

        int buyYear = getBuyingDate().get(Calendar.YEAR);
        int buyMonth = getBuyingDate().get(Calendar.MONTH);
        int buyDay = getBuyingDate().get(Calendar.DAY_OF_MONTH);

        System.out.printf("Vehicle read from file: Name: %s Color: %s Price: %s Model: %d Serialnumber: %s Direction: %d Speed: %.2f Power: %d Production date: %d-%d-%d Buying date: %d-%d-%d %n", getName(), getColor(), getPrice(), getModel(), getSerialNumber(), getDirection(), getSpeed(), getPower(), prodYear, prodMonth+1, prodDay, buyYear, buyMonth+1, buyDay);
    }

    public void writeData(PrintWriter output) throws IOException{

        int prodYear = getProductionDate().get(Calendar.YEAR);
        int prodMonth = getProductionDate().get(Calendar.MONTH);
        int prodDay = getProductionDate().get(Calendar.DAY_OF_MONTH);

        int buyYear = getBuyingDate().get(Calendar.YEAR);
        int buyMonth = getBuyingDate().get(Calendar.MONTH);
        int buyDay = getBuyingDate().get(Calendar.DAY_OF_MONTH);

        output.println(getClass().getSimpleName() + "," + getName() + "," + getColor() + "," + getPrice() + "," + getModel() + "," + getSerialNumber() + "," + getDirection() + "," + getPower() + "," + prodYear + "," + prodMonth + "," + prodDay + "," + buyYear + "," + buyMonth + "," +buyDay);
        System.out.printf("Writing vehicle to file: Name: %s Color: %s Price: %d Model: %d Serialnumber: %s Direction: %d Power: %d Production date: %d-%d-%d Buying date: %d-%d-%d %n" , getName(), getColor(), getPrice(), getModel(), getSerialNumber(), getDirection(), getPower(), prodYear, prodMonth+1, prodDay, buyYear, buyMonth+1, buyDay);
    }
}
