package com.company;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Bicycle extends Vehicle {

    private int gears;
    private Calendar productionDate;

    public Bicycle(){

    }

    public Bicycle(int gears, String color, String name, String serialNumber, int model, int price, int direction, int prodYear, int prodMonth, int prodDay){
        super(color, name, serialNumber, model, price, direction);
        this.gears = gears;
        setBuyingDate(Calendar.getInstance());
        productionDate = new GregorianCalendar(prodYear, prodMonth, prodDay);
    }

    @Override
    public void setAllFields(){
        super.setAllFields();
        System.out.println("gears: ");
        gears = input.nextInt();
        System.out.println("Production date:");
        System.out.println("Year: ");
        int year = input.nextInt();
        System.out.println("Month: ");
        int month = input.nextInt()-1;
        System.out.println("Day of month: ");
        int dayOfMonth = input.nextInt();

        GregorianCalendar date = new GregorianCalendar(year, month, dayOfMonth);
        productionDate = date;
        setBuyingDate(Calendar.getInstance());
    }

    public Calendar getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(Calendar productionDate) {
        this.productionDate = productionDate;
    }

    public int getGears() {
        return gears;
    }

    public void setGears(int gears) {
        this.gears = gears;
    }

    public void turnRight(int degrees){
        System.out.println("Bike turned " + degrees + " to the right.");
    }

    public void turnLeft(int degrees){
        System.out.println("Bike turned " + degrees + " to the left.");
    }

    @Override
    public String toString(){
        return super.toString() + "Gears: " + gears;
    }

    public void accelerate(double factor){
        if(getSpeed() == 0){
            setSpeed(0.3*factor);

            if(getSpeed()>MAX_SPEED_BIKE){
                setSpeed(MAX_SPEED_BIKE);
            }

            System.out.printf("Vehicle accelerated to: %.2f \n", getSpeed());
        }else{
            setSpeed(getSpeed()*0.5*factor);

            if(getSpeed()>MAX_SPEED_BIKE){
                setSpeed(MAX_SPEED_BIKE);
            }
            System.out.printf("Vehicle accelerated to: %.2f \n", getSpeed());
        }
    }


    public void breaks(double factor){
        setSpeed(getSpeed()/(factor*0.5));
        System.out.printf("Vehicle slowed down to: %.2f \n", getSpeed());
    }

    @Override
    public void readData(String [] parts){
        this.setName(parts[1]);
        this.setColor(parts[2]);
        this.setPrice(Integer.parseInt(parts[3]));
        this.setModel(Integer.parseInt(parts[4]));
        this.setSerialNumber(parts[5]);
        this.setDirection(Integer.parseInt(parts[6]));
        this.setGears(Integer.parseInt(parts[7]));

        GregorianCalendar prodDate = new GregorianCalendar(Integer.parseInt(parts[8]), Integer.parseInt(parts[9]), Integer.parseInt(parts[10]));
        this.setProductionDate(prodDate);

        GregorianCalendar buyDate = new GregorianCalendar(Integer.parseInt(parts[11]), Integer.parseInt(parts[12]), Integer.parseInt(parts[13]));
        this.setBuyingDate(buyDate);

        int prodYear = getProductionDate().get(Calendar.YEAR);
        int prodMonth = getProductionDate().get(Calendar.MONTH);
        int prodDay = getProductionDate().get(Calendar.DAY_OF_MONTH);

        int buyYear = getBuyingDate().get(Calendar.YEAR);
        int buyMonth = getBuyingDate().get(Calendar.MONTH);
        int buyDay = getBuyingDate().get(Calendar.DAY_OF_MONTH);

        System.out.printf("Vehicle read from file: Name: %s Color: %s Price: %s Model: %d Serialnumber: %s Direction: %d Speed: %.2f Gear: %d Production date: %d-%d-%d Buying date: %d-%d-%d%n", getName(), getColor(), getPrice(), getModel(), getSerialNumber(), getDirection(), getSpeed(), getGears(), prodYear, prodMonth+1 ,prodDay, buyYear, buyMonth+1,buyDay);
    }

    public void writeData(PrintWriter output) throws IOException{

        int prodYear = getProductionDate().get(Calendar.YEAR);
        int prodMonth = getProductionDate().get(Calendar.MONTH);
        int prodDay = getProductionDate().get(Calendar.DAY_OF_MONTH);

        int buyYear = getBuyingDate().get(Calendar.YEAR);
        int buyMonth = getBuyingDate().get(Calendar.MONTH);
        int buyDay = getBuyingDate().get(Calendar.DAY_OF_MONTH);

        output.println(getClass().getSimpleName() + "," + getName() + "," + getColor() + "," + getPrice() + "," + getModel() + "," + getSerialNumber() + "," + getDirection() + "," + getGears() + "," + prodYear + "," + prodMonth + "," + prodDay + "," + buyYear + "," + buyMonth + "," + buyDay);
        System.out.printf("Writing vehicle to file: Name: %s Color: %s Price: %d Model: %d Serialnumber: %s Direction: %d Gear: %d Production date: %d-%d-%d Buying date: %d-%d-%d %n" , getName(), getColor(), getPrice(), getModel(), getSerialNumber(), getDirection(), getGears(), prodYear, prodMonth+1, prodDay, buyYear, buyMonth+1, buyDay);
    }
}
