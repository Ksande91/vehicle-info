package com.company;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.MonthDay;
import java.util.*;

public class TestVehicles {

    public static void main(String[] args) {
        com.company.TestVehicles vtest = new com.company.TestVehicles();

        try {
            vtest.menuLoop();
        } catch (IOException e) {
            System.out.println("IO Exception!");
            System.exit(1);
        } catch (CloneNotSupportedException e) {
            System.out.println("CloneNotSupportedException");
            System.exit(1);
        }
    }

    public ArrayList<Vehicle> readVehiclesFromFile(){

        File vehicleFile = new File("C:\\Users\\Kristoffer\\IdeaProjects\\Vehicle-Info\\vehicleInfo.txt");
        ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();

        try(Scanner input = new Scanner(vehicleFile)){

            while(input.hasNext()){

                String s1 = input.nextLine();

                String[] parts = s1.split(",");

                String vehicleType = parts[0];

                Vehicle vehicle = null;

                switch (vehicleType){
                    case "Car":
                        vehicle = new Car();
                        break;
                    case "Bicycle":
                        vehicle = new Bicycle();
                }

                vehicle.readData(parts);

                vehicles.add(vehicle);
           }

        }catch (IOException ex){
            System.out.println("File doesnt exists.");
            System.out.println(ex.getMessage());
        }

        return vehicles;
    }

    public void writeVehiclesToFile(ArrayList<Vehicle>vehicles){

        File vehicleFile = new File("C:\\Users\\Kristoffer\\IdeaProjects\\Vehicle-Info\\vehicleInfo.txt");

        try(Scanner input = new Scanner(vehicleFile)){
            while (input.hasNext()){
                String s1 = input.nextLine();
                String s2 = s1.replaceAll(s1, "");
            }
        }catch (IOException e){
            System.out.println("File doesnt exists.");
            System.out.println(e.getMessage());
        }

        try(PrintWriter output = new PrintWriter(vehicleFile)){
            for(int i=0; i<vehicles.size();i++){
                vehicles.get(i).writeData(output);
            }
        }catch (IOException ex){
            System.out.println("File doesnt exists.");
            System.out.println(ex.getMessage());
        }
    }

    private void menuLoop() throws IOException, CloneNotSupportedException {

        Scanner scan = new Scanner(System.in);
        ArrayList<Vehicle> arr = readVehiclesFromFile();
        Vehicle vehicle;

        while (true) {
            System.out.println("1...................................New car");
            System.out.println("2...............................New bicycle");
            System.out.println("3......................Find vehicle by name");
            System.out.println("4..............Show data about all vehicles");
            System.out.println("5.......Change direction of a given vehicle");
            System.out.println("6.........................Test Clone method");
            System.out.println("7...................Test drivable interface");
            System.out.println("8..............................Exit program");
            System.out.println(".............................Your choice?");
            int choice = scan.nextInt();

            switch (choice) {
                case 1:
                    Car car = new Car();
                    car.setAllFields();
                    arr.add(car);
                    break;
                case 2:
                    Bicycle bicycle = new Bicycle();
                    bicycle.setAllFields();
                    arr.add(bicycle);
                    break;
                case 3:
                    System.out.println("Name of vehicle: ");
                    scan.nextLine();
                    String vehicleCompare =  scan.nextLine();

                    for(int i = 0; i<arr.size(); i++){

                        if(arr.get(i).getName().equalsIgnoreCase(vehicleCompare)){
                            System.out.println(arr.get(i).toString());
                        }
                    }

                    break;
                case 4:
                    for(int i =0; i<arr.size(); i++){
                        System.out.println(arr.get(i).toString());
                    }
                    break;
                case 5:
                    System.out.println("Name of vehicle: ");
                    scan.nextLine();
                    String name2 = scan.nextLine();
                    System.out.println("Direction:");
                    String direction = scan.nextLine();
                    System.out.println("Degrees [0-360]:");
                    int degrees = scan.nextInt();

                    for(int i=0; i<arr.size(); i++){
                        if(arr.get(i).getName().equalsIgnoreCase(name2)){
                            if(direction.equalsIgnoreCase("left")){
                                arr.get(i).turnLeft(degrees);
                            }
                            else if (direction.equalsIgnoreCase("right")){
                                arr.get(i).turnRight(degrees);
                            }
                        }
                    }
                    break;
                case 6:
                    arr.add(new Car(169, "Blue", "Lada", "1010-13", 2011, 300000, 0, 1998,9,15));
                    Vehicle clonedCar = arr.get(arr.size()-1).clone();
                    clonedCar.getBuyingDate().set(1999, 03, 15);
                    System.out.println("Date objects are separated, deep copy.");
                    System.out.println("First created object: " + arr.get(arr.size()-2).getBuyingDate().getTime());
                    System.out.println("Cloned object: " +arr.get(arr.size()-1).getBuyingDate().getTime());
                    break;
                case 7:
                    System.out.println("Car:");
                    arr.add(new Car(170, "Grey", "Audi", "1010-14", 2012, 400000, 0,2011,11,12));
                    Vehicle testCar = arr.get(arr.size()-1);
                    testCar.accelerate(10);
                    testCar.accelerate(20);
                    testCar.breaks(12);
                    testCar.stop();

                    System.out.println("Bicycle:");
                    arr.add(new Bicycle(1, "Green", "BMX", "42t", 1997, 2000, 0,1996,8,9));
                    Vehicle testBike = arr.get(arr.size()-1);
                    testBike.accelerate(10);
                    testBike.accelerate(20);
                    testBike.breaks(12);
                    testBike.stop();

                    break;
                case 8:scan.close();
                    writeVehiclesToFile(arr);
                    System.exit(0);
                default:System.out.println("Wrong input!");}}
    }


}
