package com.company;

import java.awt.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public abstract class Vehicle implements Comparable<Vehicle>, Cloneable, Drivable, Fileable{
    private String color, name, serialNumber;
    private int model, price, direction;
    private double speed;
    private Calendar buyingDate;

    protected Scanner input = new Scanner(System.in);

    public Vehicle(){

    }

    @Override

    public int compareTo(Vehicle o){
        if(getPrice() > o.getPrice()){
            return 1;
        }else if(getPrice()<o.getPrice()){
            return -1;
        }else{
            return 0;
        }
    }

    public Calendar getBuyingDate() {
        return buyingDate;
    }

    public void setBuyingDate(Calendar buyingDate) {
        this.buyingDate = buyingDate;
    }

    @Override

    public Vehicle clone()throws CloneNotSupportedException{
        Vehicle cloned = (Vehicle)super.clone();
        cloned.setBuyingDate((Calendar)cloned.getBuyingDate());
        return cloned;
    }

    public Vehicle(String color, String name, String serialNumber, int model, int price, int direction){
        this.color = color;
        this.name = name;
        this.serialNumber = serialNumber;
        this.model = model;
        this.price = price;
        this.direction = direction;
        buyingDate = Calendar.getInstance();
    }

    public void writeData(PrintWriter output) throws IOException{
        output.println(getName() + "," + getColor() + "," + getPrice() + "," + getModel() + "," + getSerialNumber() + "," + getDirection());
        System.out.printf("Writing vehicle to file: Name: %s  Color: %s Price: %d Model: %d Serialnumber: %s Direction: %d Power: %d %n" , getName(), getColor(), getPrice(), getModel(), getSerialNumber(), getDirection());
    }


    public void readData(String[] parts){

        this.setName(parts[1]);
        this.setColor(parts[2]);
        this.setPrice(Integer.parseInt(parts[3]));
        this.setModel(Integer.parseInt(parts[4]));
        this.setSerialNumber(parts[5]);
        this.setDirection(Integer.parseInt(parts[6]));

    }

    public void setAllFields(){
        System.out.println("Color: ");
        this.color = input.nextLine();
        System.out.println("Name: ");
        this.name = input.nextLine();
        System.out.println("Serial number: ");
        this.serialNumber = input.nextLine();
        System.out.println("Model: ");
        this.model = input.nextInt();
        System.out.println("Price: ");
        this.price = input.nextInt();
    }

    public abstract void turnLeft(int degrees);

    public abstract void turnRight(int degrees);


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public int getModel() {
        return model;
    }

    public void setModel(int model) {
        this.model = model;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }



    public void stop(){
        System.out.println("Vehicle stops.");
        speed = 0;
    }

    @Override

    public String toString(){
        return String.format("--------------------------- %nName: %s %nColor: %s %nSerial number: %s %nModel: %d %nPrice: %d %nDirection: %d %n", name, color, serialNumber, model, price, direction);
    }
}
